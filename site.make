core = 7.x
api = 2

; uw_open_data_important_dates
projects[uw_open_data_important_dates][type] = "module"
projects[uw_open_data_important_dates][download][type] = "git"
projects[uw_open_data_important_dates][download][url] = "https://git.uwaterloo.ca/wcms/uw_open_data_important_dates.git"
projects[uw_open_data_important_dates][download][tag] = "7.x-1.4"
